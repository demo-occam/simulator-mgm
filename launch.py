import os
import json
# Helper file
from occam import Occam

#Get information about object
object = Occam.load()
#####################################
#           Gather paths            #
#####################################
scripts_path = os.path.dirname(__file__)
job_path = os.getcwd()
# Path where the object directory is mounted
object_path = "/occam/%s-%s"%(object.id(),object.revision())

#####################################
#          Set variables            #
#####################################
runner_script = os.path.join(object_path, "MGM.jar")
build_folder =  os.path.join(object_path, "build")

# Default program
input_file = os.path.join(object_path,"test")
# Get input from OCCAM
inputs = object.inputs("data/TSV")
if len(inputs) > 0:
    files = inputs[0].files()
    if len(files) > 0:
        input_file = files[0]


# Default configuration parameters
#simulation_configuration = os.path.join(object_path,"default_simulation.json")
# Get configuration parameters from OCCAM
simulation_configuration = object.configuration("MGM configuration")
lambda1=simulation_configuration.get("Lambda_1")
lambda2=simulation_configuration.get("Lambda_2")
lambda3=simulation_configuration.get("Lambda_3")
alpha=simulation_configuration.get("Alpha")
if simulation_configuration.get("PCS-Used"):
    pcs_used="-pcs"
else:
    pcs_used=""


# Output file
output_file="output.json"
# Get output path from OCCAM (inputs <type of output>, <output directory>, <output filename>)
output_file = object.output_path("application/json", "output_dir", "output.json", "MGM output")
output_generated_file = object.output_generated_path("application/json", "output_dir", "output.json", "MGM output")

#Setup run command
run_sim_command= [
	"java -jar ", runner_script,
	" -d ", input_file,
	" -o ", output_file,
    " -l %f %f %f"%(lambda1,lambda2,lambda3),
    " -a %f"%(alpha),
    pcs_used
]
if(output_generated_file!=None):
    run_sim_command.append("; cp %s %s"%(output_file, output_generated_file))

command = ' '.join(run_sim_command)

# Pass run command to OCCAM
Occam.report(command)
